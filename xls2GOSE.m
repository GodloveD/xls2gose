function xls2GOSE
% read a spreadsheet containing GOSE scoring data, code it according to
% the given algorithm, and write it back out to a new spreadsheet.
% Assumes Windows and requires that Microsoft Excel be installed on the 
% system. 
% author - David Godlove
% date   - Tue, Jun 14, 2016 12:25:34 PM
% email  - davidgodlove@gmail.com

% open up a dialog box for input
filepath = pwd;
[filename, filepath] = uigetfile({'*.xlsx';'*.xls'},'Select spreadsheet to analyze.',filepath);

% parse input
[~, filename, extension] = fileparts(filename);

% and another for output
[ofile, opath] = uiputfile({'*.xlsx';'*.xls'},'Select spreadsheet to write.', filepath);

% read the spreadsheet
% ~~~~~~~~~~~~~~~~~~~~~~~~~~
COMPLETION = xls2struct(filepath, [filename extension]);

% implement the algorithm
% order is reversed in magnitude so that smaller scores overwrite larger
% scores.  that way no min needs to be taken at the end.
% ~~~~~~~~~~~~~~~~~~~~~~~~~~
GOSE(length(COMPLETION.GUID), 1) = zeros;

% % UPPER GR (needs to be removed to make a general tool)
% some_guids = {'TBI_INVAH691PPK',...
%     'TBI_INVFK848GJB',...
%     'TBI_INVUE493HPL',...
%     'TBI_INVPN091ADQ'};
% for ii = 1:length(some_guids)
%     GOSE(strcmp(some_guids{ii}, COMPLETION.GUID)) = 8;
% end

% UPPER GR
GOSE(strcmp('No', COMPLETION.CrrntProbInd) & ...
    ~strcmp('Yes', COMPLETION.SimlrProbPreInjryInd)) = 8;

% LOWER GR
GOSE( (strcmp('Participate a bit less: at least half as often as before injury', COMPLETION.ExtntRestrctSocFreq) & ...
    ~strcmp('No', COMPLETION.ExtntRestrctSocPreInjrInd)) |...
    (strcmp('Occasional (less than weekly)', COMPLETION.ExtntFriendshpStrnFreq) & ...
    ~strcmp('Yes', COMPLETION.LvlStrnPreInjryInd))       |...
    (strcmp('Yes', COMPLETION.CrrntProbInd) & ...
    ~strcmp('Yes', COMPLETION.SimlrProbPreInjryInd)) ...
    ) = 7;

% UPPER MD
GOSE( (strcmp('Reduced work capacity', COMPLETION.WrkRestrictTyp) & ...
    ~strcmp('No', COMPLETION.LvlRestrictInd))            |...
    (strcmp('Participate much less: less than half as often', COMPLETION.ExtntRestrctSocFreq) & ...
    ~strcmp('No', COMPLETION.ExtntRestrctSocPreInjrInd)) |...
    (strcmp('Frequent (once a week or more, but tolerable)', COMPLETION.ExtntFriendshpStrnFreq) & ...
    ~strcmp('Yes', COMPLETION.LvlStrnPreInjryInd)) ...
    ) = 6;

% LOWER MD
GOSE( (strcmp('Able to work only in a sheltered workshop or non-competitive job or currently unable to work', COMPLETION.WrkRestrictTyp) & ...
    ~strcmp('No', COMPLETION.LvlRestrictInd))            |...
    (strcmp('Unable to participate: rarely, if ever, take part', COMPLETION.ExtntRestrctSocFreq) & ...
    ~strcmp('No', COMPLETION.ExtntRestrctSocPreInjrInd))  |...
    (strcmp('Constant (daily and intolerable)', COMPLETION.ExtntFriendshpStrnFreq) & ...
    ~strcmp('Yes', COMPLETION.LvlStrnPreInjryInd)) ...
    ) = 5;

% UPPER SD
GOSE( (strcmp('No', COMPLETION.FreqntHlpNeedInd) & ...
    ~strcmp('Yes', COMPLETION.IndpntPreInjryInd))        |...
    (strcmp('No', COMPLETION.ShpWoutAsstInd) & ...
    ~strcmp('No', COMPLETION.ShpWoutAsstPreInjryInd))    |...
    (strcmp('No', COMPLETION.TrvlWoutAsstInd) & ...
    ~strcmp('No', COMPLETION.TrvlWoutAsstPreInjryInd)) ...
    )  = 4;

% LOWER SD
GOSE(strcmp('Yes', COMPLETION.FreqntHlpNeedInd) & ...
    ~strcmp('Yes', COMPLETION.IndpntPreInjryInd)) = 3;

% VS
GOSE(strcmp('No', COMPLETION.ConsciousInd)) = 2;

% write out a new spreadsheet
% ~~~~~~~~~~~~~~~~~~~~~~~~~~
output.GUID = COMPLETION.GUID;
output.GOSE = GOSE;
struct2xls(output, opath, ofile)


function xls_struct = xls2struct(filepath, filename)
% xls2struct converts an excel spreadsheet to a matlab data structure.  It
% assumes that the first row in the spreadsheet will contain headers (not
% data) and that the spreadsheet will contain simple numerical and
% character data (no formulas or fancy stuff).  The heading from each
% column will become the fieldname and the remainder of the column will
% contain values.
%
% This version has been modified to deal specifically with GOSE spreadsheet
% data
%
% author - David Godlove
% date   - Tue, Jun 14, 2016 12:25:34 PM
% email  - davidgodlove@gmail.com
%
% see also struct2xls

params = {'Response'            ;...
    'ConsciousInd'              ;...
    'AsstNeedInd'               ;...
    'FreqntHlpNeedInd'          ;...
    'IndpntPreInjryInd'         ;...
    'ShpWoutAsstInd'            ;...
    'ShpWoutAsstPreInjryInd'    ;...
    'TrvlWoutAsstInd'           ;...
    'TrvlWoutAsstPreInjryInd'   ;...
    'CrrntWrkInd'               ;...
    'WrkRestrictTyp'            ;...
    'LvlRestrictInd'            ;...
    'SocLeisOutInd'             ;...
    'ExtntRestrctSocFreq'       ;...
    'ExtntRestrctSocPreInjrInd' ;...
    'FrindshpInd'               ;...
    'ExtntFriendshpStrnFreq'    ;...
    'LvlStrnPreInjryInd'        ;...
    'CrrntProbInd'              ;...
    'SimlrProbPreInjryInd'      ;...
    'MstImptFctrTyp'            ;...
    'GlasgowOutcomeScalExtScore'};

[~, head_check, data] = xlsread(fullfile(filepath,filename));

% where is the header? (ROW that contains the text 'GUID')
[head_check, ~] = find(~cellfun(@isempty, strfind(head_check, 'GUID')));
header_row = head_check(1);

found(size(params)) = false;
found_guid = false;

fields = data(header_row,:);
data   = data(header_row+1:end,:);

% only save fields with one of the need params in them.  check that each
% param is saved once and only once.  (note that this loop runs in reverse
% because we are deleting elements)
for ii = length(fields):-1:1
    
    this_field = fields{ii};
    
    delete_one = true;
    for jj = 1:length(params)
        
        % look for GUID and  continue if we find it
        if strcmp(strtrim(this_field), 'GUID')
            fields{ii} = 'GUID';
            if found_guid == true
                error('ERROR: More than one GUID field exists in spreadsheet.')
            end
            found_guid = true;
            delete_one = false;
            break
        end
        
        % look for other params
        this_param = params{jj};
        if ~isempty(regexp(this_field, this_param, 'Once'));
            fields{ii} = this_param;
            if found(jj) == true
                error('ERROR: one or more parameter exists multiple times in the spreadsheet.')
            end
            found(jj) = true;
            delete_one = false;
        end
    end
    % if we didn't match any of the key params to this column, delete it
    if delete_one
        fields(ii) = [];
        data(:,ii) = [];
    end
end

if any(found == false) || found_guid == false
    error('ERROR: one or more parameters not present in spreadsheet.')
end

% populate the struct with data in the appropriate fields
for ii = 1:length(fields)
    
    this_col = data(:,ii);
    char_data = sum(cellfun(@ischar,this_col));
    
    if char_data == false               % if no chars are found
                                        % then we assume this is all numerical (double) data
        this_col = cell2mat(this_col);  % and we convert the column (cell array) to a vector of doubles
    end                                 % otherwise just leave it as a cell array
    
    xls_struct.(fields{ii}) = this_col;
    
end

function struct2xls(data_struct,filepath,filename)
% struct2xls converts a matlab data structure to an excel spreadsheet.  It
% uses the fieldnames of the data structure as a header at the top of each
% column of the spreadsheet and places the contents of each field
% underneath.  It assumes that each field will contain a one dimensional
% vector of doubles or a 1D cell array with character data.
%
% author - David Godlove
% date   - Tue, Jun 14, 2016 12:25:34 PM
% email  - davidgodlove@gmail.com
%
% see also xls2struct

header_row   = 1;
fields       = fieldnames(data_struct);
ascii_num    = double('A'); %A (for columns)
ascii_append = 1; %tells you how many characters to draw (for instance AA)
column_ct    = 0;
for ii = 1:length(fields)
    
    column_ct   = column_ct + 1;
    column_name = fields(ii);
    for jj = 1:length(ascii_append)
        column_letter(jj) = char(ascii_num);
    end
    
    % figure out where the header should go
    header_index = sprintf('%s%s:%s%s',column_letter,num2str(header_row),column_letter,num2str(header_row));
    
    % write the header in for the current column
    xlswrite(fullfile(filepath, filename), column_name, header_index)
    
    % get the current data
    eval(sprintf('curr_data = data_struct.%s;',char(column_name)))
    
    % figure out where it should go
    data_index = sprintf('%s%s:%s%s',column_letter,num2str(header_row + 1),column_letter,num2str(header_row + length(curr_data)));
    
    % write the data in for the current column
    xlswrite(fullfile(filepath, filename), curr_data, data_index) 
    
    % this mechaninms gives you columns A-Z and then starts AA, AAA and so
    % forth
    if ascii_num == double('Z')
        ascii_num = double('A');
        ascii_append = ascii_append + 1;
    else
        ascii_num = ascii_num + 1;
    end
    
end